# Examples of basic concepts in Elixir
# Based on: https://www.youtube.com/watch?v=pBNOavRoNL0

# Try it:
#   sh$ iex -r tutorial.ex -e Tut.main -e System.halt
# or
#   sh$ elixir -r tutorial.ex -e Tut.main
# or
#   iex> c("tutorial.ex")
#   iex> Tut.main

# Commands to exit the REPL (iex):
#   (command)  :c.q
#   (command)  System.halt
#   (keybind)  C-c C-c
#   (keybind)  C-c a
#   (keybind)  C-g q

# Compile and run
#   sh$ elixirc tutorial.ex
#   iex> import(Tut)
#   iex> Tut.main()


defmodule Tut do
  def main do
    data_stuff()
  end

  def data_stuff do

    # sth = IO.gets("Write something... ") |> String.trim
    # IO.puts sth
    
    IO.puts "# String interpolation"
    IO.puts "Atom #{is_atom(:Warsaw)}"

    IO.puts "# Strings"
    str = "Sample string"
    conc_str = str <> " " <> "+ some stuff."
    IO.puts String.capitalize(conc_str)
    IO.puts String.downcase(conc_str)
    IO.puts String.reverse(conc_str)
    IO.puts String.upcase(conc_str)

    IO.puts "# |>"
    2 * 3 |> IO.puts

    IO.puts "# Comparision"
    IO.puts "4 == 4.0 : #{4 == 4.0}"
    IO.puts "4 === 4.0 : #{4 === 4.0}"
    IO.puts "4 !== 4.0 : #{4 == 4.0}"
    IO.puts "4 !=== 4.0 : #{4 === 4.0}"

    age = 16

    IO.puts "# If-else"
    if age >= 18 do
      IO.puts "Can vote"
    else
      IO.puts "Can't vote"
    end
    
    IO.puts "# Unless"
    unless age === 18 do
      IO.puts "You are not 18"
    else
      IO.puts "You are 18"
    end

    IO.puts "# Cond"
    cond do
      age >= 18 -> IO.puts "You can vote"
      age >= 16 -> IO.puts "You can drive"
      age >= 14 -> IO.puts "You can wait"
      true -> IO.puts "Default"
    end

    IO.puts "# Case"
    case 2 do
      1 -> IO.puts "Entered 1"
      2 -> IO.puts "Entered 2"
      _ -> IO.puts "Default"
    end

    IO.puts "# Tuples"
    stats = {1.0, 2.4, 6.9}
    stats2 = Tuple.append(stats, 4.5)
    IO.puts "First #{elem(stats2, 0)}"
    IO.puts "Size #{tuple_size(stats2)}"
    Tuple.delete_at(stats2, 0)
    Tuple.insert_at(stats2, 0, 3.0)
    Tuple.duplicate(0, 5)
    {weight, height, name} = {175, 6.5, "Mark"}
    IO.puts "Weight : #{weight}"

    IO.puts "# Lists"
    list1 = [1, 2, 3]
    list2 = [4, 5, 6]
    list3 = list1 ++ list2
    list4 = list1 -- list2
    IO.puts 6 in list4
    [head | tail] = list3
    IO.puts "Head : #{head}"
    IO.write "Tail : "
    IO.inspect tail
    IO.inspect [97, 98]
    IO.inspect [97, 98], charlists: :as_lists
    Enum.each tail, fn item ->
      IO.puts item
    end

    IO.puts "# Recursion"
    IO.puts display_list(list4)

    IO.puts "# Maps (Dictionaries)"
    capitals = %{
      "Alabama" => "Montgomery",
      "Alaska" => "Juneau"
    }
    IO.puts "#{capitals["Alaska"]}"
    capitals2 = %{
      alabama: "Montgomery",
      alaska: "Juneau"
    }
    IO.puts "#{capitals2.alaska}"

    IO.puts "# Lambda"
    get_sum = fn (x, y) -> x + y end
    get_dif = &(&1 - &2)
    IO.puts "5 + 5 = #{get_sum.(5, 5)}"
    IO.puts "5 - 5 = #{get_dif.(5, 5)}"
    add_sum = fn
      {x, y} -> IO.puts "#{x} + #{y} = #{x + y}"
      {x, y, z} -> IO.puts "#{x} + #{y} + #{y} = #{x + y + z}"
    end
    add_sum.({1, 2})
    add_sum.({1, 2, 3})

    IO.puts "# Enum (actually 'map')"
    IO.puts "Even list : #{Enum.any?([1, 2, 3], fn(n) -> rem(n, 2) == 0 end)}"
    Enum.each([1, 2, 3], fn(n) -> IO.puts n end)
    x2_list = Enum.map([1, 2, 3], fn(n) -> n * 2 end)
    IO.inspect x2_list
    enum_sum = Enum.reduce([1, 2, 3], fn(n, sum) -> n + sum end)
    IO.puts "Sum : #{enum_sum}"
    IO.inspect Enum.uniq([1, 2, 3])

    IO.puts "# List comprehension"
    dbl_list = for n <- [1, 2, 3], do: n * 2
    IO.inspect dbl_list

    IO.puts "# Exception handling"
    err = try do
            5 / 0
          rescue
            ArithmeticError -> "Division by zero!"
          end

    IO.puts "# Concurrency"
    spawn(fn() -> display_list(list1) end)
    spawn(fn() -> display_list(list2) end)
    spawn(fn() -> display_list(list3) end)
    spawn(fn() -> display_list(list4) end)
    send(self(), {:french, "Bob"})
    receive do
      {:english, name} -> IO.puts "Hello #{name}"
      {:french, name} -> IO.puts "Bonjour #{name}"
      {:german, name} -> IO.puts "Guten tag #{name}"
    after
      500 -> IO.puts "Time up"
    end
    
  end

  # Function definitions can't go into another function definition (main)
  def display_list([word | words]) do
    IO.puts word
    display_list(words)
  end
  def display_list([]), do: nil
  
end
