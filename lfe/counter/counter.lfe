;;; Demo: Counter App
;; From: http://docs.lfe.io/current/tutorials/counter-app/2.html

;;; Running:
;; > (c '"counter.lfe")
;; => (#(module counter))
;; > (set foo (: counter new))
;; => <0.92.0>
;; > (: counter count foo)
;; => 0
;; > (: counter incr foo)
;; => #(incr)
;; > (: counter incr foo)
;; => #(incr)
;; > (: counter count foo)
;; => 2


(defmodule counter
  (export
   (new 0)
   (incr 1)
   (count 1)
   (counter 0)
   )
  )

(defun new ()
  "For creating a new counter."
  (spawn 'counter 'counter '())
  )

(defun incr (counter)
  "Increments our counter value."
  (! counter (tuple 'incr))
  )

(defun count (counter)
  "Retrieves the value of the given counter."
  (! counter (tuple 'count (self)))
  (receive
    (
     (tuple 'count count)
     count
     )
    )
  )

(defun counter ()
  "Runs our actual counter loop."
  (counter-loop 0)
  )

(defun counter-loop (count)
  (receive
    (
     (tuple incr)
     (counter-loop (+ count 1))
     )
    (
     (tuple 'count requestor)
     (! requestor (tuple 'count count))
     (counter-loop count)
     )
    )
  )
